/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  publicRuntimeConfig: {
    partnerId: process.env.PARTNER_ID || 'palla',
  },
};

module.exports = nextConfig;
