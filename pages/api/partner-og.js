import { ImageResponse } from '@vercel/og';

export const config = {
  runtime: 'edge',
};

export default function handler(req) {
  try {
    const { searchParams } = new URL(req.url);
    const name = searchParams.get('name') || 'Random Name';
    const country = searchParams.get('country') || 'Random Country';
    const partnerId = searchParams.get('partnerId') || 'palla';
    const appId = searchParams.get('appId') || 'checkout';
    const previewSrc = `https://palla-images.s3.us-east-2.amazonaws.com/partners/${partnerId}/${appId}/preview.png`;
    const titleColor = searchParams.get('titleColor') || 'yellow';
    const title = searchParams.get('title') || partnerId;
    const textColor = searchParams.get('textColor') || 'white';
    const description = `Send money to ${name} in ${country} in seconds`;
    return new ImageResponse(
      (
        <div
          style={{
            backgroundColor: 'black',
            backgroundSize: '150px 150px',
            height: '100%',
            width: '100%',
            display: 'flex',
            textAlign: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
            flexWrap: 'nowrap',
          }}
        >
          <img style={{ width: '100%', height: '100%' }} src={previewSrc} />
          <div
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'center',
              height: '100%',
              width: '100%',
            }}
          >
            <div
              style={{
                display: 'flex',
                height: '35%',
                justifyContent: 'flex-start',
                alignItems: 'flex-end',
                width: '100%',
                paddingLeft: '90px',
              }}
            >
              <h1
                style={{
                  fontSize: 40,
                  color: titleColor,
                  textTransform: 'uppercase',
                }}
              >
                {title}
              </h1>
            </div>
            <div
              style={{
                display: 'flex',
                height: '75%',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                width: '100%',
                paddingLeft: '90px',
                paddingRight: '50px',
                textAlign: 'left',
              }}
            >
              <p style={{ fontSize: 50, color: textColor }}>{description}</p>
            </div>
          </div>
        </div>
      ),
      {
        width: 900,
        height: 600,
      }
    );
  } catch (e) {
    console.log(`${e.message}`);
    return new Response(`Failed to generate the image`, {
      status: 500,
    });
  }
}
